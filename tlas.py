#This program is a submission for contest in the Udacity's CS101 class and as such is released under a Creative Commons CC BY-NC-SA license.

import re
import sys

def reverse_index(dict):
   new_dict = {}
   element_list = []
   
   for key in dict:
      if dict[key] not in element_list:
	 element_list.append(dict[key])
   for element in element_list:
      if element not in new_dict:
	 new_dict[element] = ""
      
      for key in dict:
	 if (dict[key] == element) and (key not in new_dict[element]):
	    new_dict[element] = key
   
   return new_dict

letter_to_pron = {
"a" : "ei",
"b" : "bi",
"c" : "si",
"d" : "di",
"e" : "i",
"f" : "ef",
"i" : "ai",
"k" : "kei",
"l" : "el",
"m" : "em",
"n" : "en",
"o" : "ou",
"p" : "pi",
"q" : "kyu",
"r" : "ar",
"s" : "es",
"t" : "ti",
"u" : "yu",
"v" : "vi",
"x" : "eks",
"y" : "wai",
"z" : "zi"
}
pron_to_letter = reverse_index(letter_to_pron)

number_to_pron = {
"1" : "wan",
"2" : "tu",
#3 -> not sure how to write it
"4" : "for",
"5" : "faiv",
"6" : "siks",
"7" : "sevn",
"8" : "eit",
"9" : "nain"
}
pron_to_number = reverse_index(number_to_pron)

word_to_pron = {
"any" : "eni",
"are" : "ar",
"ate" : "eit",
"be" : "bi",
"before" : "bifor",
"done" : "dan",
"eye" : "ai",
"for" : "for",
"hate" : "heit",
"hatred" : "heitred",
"ing" : "in",
"mating" : "meitin",
"one" : "wan",
"see" : "si",
"some" : "sam",
"stop" : "stap",
"tea" : "ti",
"to" : "tu",
"up" : "ap",
"wait" : "weit",
"water" : "woter",
"what" : "wat",
"why" : "wai",
"you" : "yu"
}
pron_to_word = reverse_index(word_to_pron)

def split_string(source, splitlist):
   matches = re.findall(splitlist,source)
   return matches

#new understanding of how "english" works. Numbers can go anywhere because it's how they are read. Though letters can go only if they appear once (to simplify problem gonna keep it at one), maybe max 2 times.
#Any more than that and they are read as collection of letters, not as individual letters

def count_alpha(word):
   alpha_number = 0
   for character in word:
      if character.isalpha():
	 alpha_number += 1
   return alpha_number

def count_caps(word):
   caps_number = 0
   for character in word:
      if character.isupper():
         caps_number += 1
   return caps_number

#simplification of a word based on it's pronunciation. => someone -> samwan -> sam1 -> some1; before -> bifor -> bi4 -> b4
def simplify_pron(word):
   pron = word_to_pron[word]
   
   #if it's just a simple word => to -> 2 it can be dealt with fast
   if pron in pron_to_letter:
      return pron_to_letter[pron]
   if pron in pron_to_number:
      return pron_to_number[pron]
   
   #if it's not we'll try to extract something based on it's pronunciation
   pron_len = len(pron)
   
   #search for an instance that we can exchange for a number, "eit" -> 8
   i = 0
   while i < pron_len:
      j = 1
      while j + i < pron_len + 1:
	 if pron[i:i + j] in pron_to_number:
	    pron = pron.replace(pron[i:i + j], pron_to_number[pron[i:i + j]], 1)
	    pron_len = len(pron)
	    break #tonight = t... to -> 2 -> 2night, break because there is no need to search for combinations of "2ni" and so on
	 j += 1
      i += 1

   #counting alphabetic characters. Because I kicked some, max length is 3 characters. Meaning that by the rule of (because of simplification) max one character in word if we have 4 alphabetic
   #char's and "biggest" conversion we can get is 3 chars long, we can just skip this step because otherwise we can get 2 or more characters which can make "reading with understanding" difficult
   if count_alpha(pron) < 4:
      #no need to recalc pron_len since we did that in previous while loop (if if doesn't succeed, then first len counts, if it does, then len in it counts)
      #search for instance that we can exchange for a letter. Accept that change only if there is one letter at the end of conversion
      #doing only one while pass because if we don't find the replacement in first go, there will be more than one character anyway so no need to do multiple passes
      i = 0
      j = -1 #can't set it to 0 because first element might be at 0'th position
      while i < pron_len:
	 if pron[i].isalpha() and j == -1:
	    j = i
	 elif not pron[i].isalpha() and j != -1:
	    tmp_pron = ""
	    if pron[j:i] in pron_to_letter:
	       tmp_pron = pron.replace(pron[j:i], pron_to_letter[pron[j:i]], 1)

	    if count_alpha(tmp_pron) == 1:
	       return tmp_pron #if there is only one char that means that we have a combination simliar to b4... so no need to further process it
	    else:
	       break
	 i += 1
   
   #some intuition might point out that this step is needed in case that we have 4 or more chars but it is needed if we have less too. Case of "sam1", it won't be replaced by... s1, but stay sam1
   #so in this step, we try to revert pronunciation back to spelling -> some1
   i = 0
   while i < pron_len:
      j = 0
      while i + j < pron_len + 1:
	 if pron[i:i + j] in pron_to_word:
	    pron = pron.replace(pron[i:i + j], pron_to_word[pron[i:i + j]], 1)
	 j += 1
      i += 1

   return pron
   

def simplify_word(word):
   #if word is in our "dictionary"
   if word in word_to_pron:
      poss_candidate = simplify_pron(word)

      if poss_candidate != word:
	 return poss_candidate
      else:
	 return word
   else:
      #if word is not in dictionary
      word_len = len(word)
      #get subwords in case one of them can be simplified, like with "waiting", should be cut down to "wait" and "ing" eventually where we'd get back w8 and ing, and connect them to w8ing
      i = 0
      while i < word_len:
	 j = 1
	 while j + i < word_len + 1:
	    subword = word[i:j + i]#don't know... maybe it's faster to "fetch" it once than twice
	    #no need to deal with word if it's the same word as the one at start since we already did that once. Also later we don't need to simplify 2night but subsets of that word
	    if subword in word_to_pron and subword != word:
	       replace = simplify_pron(subword)
	       
	       if replace != subword:
		  digit_change = False
		  for element in replace:
		     if element.isdigit():
			digit_change = True #case of waiting. w8 is digit change and we can accept it, but in case of "care" we end up with "cr" so if change doesn't have digits, we don't accept it

		  tmp_word = word.replace(subword, replace, 1) #1 so it wouldn't do more than one replace
		  if digit_change:
		     word = tmp_word
		     word_len = len(word)
		     break #to break from first loop... waiting -> if it gets to wait-> w8 -> w8ing, we don't need to try to compare w8i to something, but continue from next subset
	    j += 1

	 i += 1

      return word

def simplify_line(line):
   split_line = line.split()
   
   #don't know if it recalcs range of len of split_line every time it loops, so this might be faster
   split_line_len = range(len(split_line))
   for i in split_line_len:
      word = split_string(split_line[i], "\w+'?\w")

      for element in word:
	 new_word = simplify_word(element.lower())

	 if new_word != element and new_word not in word_to_pron:
	    #replace only if new found word isn't already existing one... don't want to switch something like "before" with "for" and then try to figure out how to return it back
	    #don't want to caps "i" into "I" and then be confused if it was an "I" as "me" or "i" as in "eye"
	    if count_caps(element) > 1:
               tmp_word = new_word
               new_word = ""
               for letter in tmp_word:
                  new_word += letter.capitalize()
	    elif (element[0].isupper() and new_word != "i") or element == "I":
	       new_word = new_word.capitalize()

	    split_line[i] = split_line[i].replace(element, new_word, 1) #replace only one occurance of it.

   return " ".join(split_line)


def reconstruct(word, alpha_count = 1):
   #if it's simple conversion, just return it
   if alpha_count == 1 and word in letter_to_pron and letter_to_pron[word] in pron_to_word:
      return pron_to_word[letter_to_pron[word]]
   if word in number_to_pron and number_to_pron[word] in pron_to_word:
      return pron_to_word[number_to_pron[word]]

   #if it's not we'll try to extract something based on it's pronunciation
   word_len = len(word)
   alpha_count = count_alpha(word)
   
   poss_cand = [word]
   if alpha_count == 1:
      i = 0
      while i < word_len:
         if word[i].isalpha():
            if word[i] in letter_to_pron:
               poss_cand.append(word.replace(word[i], letter_to_pron[word[i]], 1))
               break;
         i += 1

   #search for an instance that we can exchange for a number
   #idea, "m8"-> ['m8', 'em8'] -> pop(m8) -> [em8], append one for pronun and one for direct tranlation -> [em8, meit, mate], pop(em8), append one for pronun and one for direct trans -> [meit, mate, emeit, emate]
   poss_cand_len = len(poss_cand)
   i = 0
   while i < poss_cand_len:
      candidate = poss_cand.pop(0)
      cand_len = len(candidate)
      j = 0
      while j < cand_len:
         if candidate[j] in number_to_pron:
            poss_cand.append(candidate.replace(candidate[j], number_to_pron[candidate[j]], 1))
            if number_to_pron[candidate[j]] in pron_to_word:
               poss_cand.append(candidate.replace(candidate[j], pron_to_word[number_to_pron[candidate[j]]], 1))
         j += 1
      i += 1

   #idea, case of m8ing -> meitin, so ['meiting', 'mateing'] -> ['meiting', 'mateing', 'meitin', 'meiting', 'matein']
   i = 0
   poss_cand_len = len(poss_cand)
   while i < poss_cand_len:
      candidate = poss_cand[i]
      cand_len = len(candidate)
      j = 0
      while j < cand_len:
         k = 0
         while k + j < cand_len + 1:
            if candidate[j:j + k] in word_to_pron:
               poss_cand.append(candidate.replace(candidate[j:j + k], word_to_pron[candidate[j:j + k]], 1))
            k += 1
         j += 1
      i += 1

   #return first that matches a result.
   for candidate in poss_cand:
      if candidate in word_to_pron:
         return candidate
      if candidate in pron_to_word:
         return pron_to_word[candidate]
   
   #else, return a word
   return word

def reconstruct_word(word):
   poss_candidate = reconstruct(word)
   alpha_count = count_alpha(word)

   if poss_candidate != word and poss_candidate in word_to_pron:
      return poss_candidate
   else:
      #if word is not in dictionary
      word_len = len(word)
      #get subwords in case one of them can be reconstructed, like with w8ing, should be cut down to "w8" and "ing" eventually, where we'd get back wait and ing, and connect them to waiting
      i = 0
      while i < word_len:
	 j = 1
	 while j + i < word_len + 1:
	    subword = word[i:j + i]#don't know... maybe it's faster to "fetch" it once than twice
	    #no need to deal with word if it's the same word as the one at start since we already did that once. Also later we don't need to simplify 2night but it's subsets of that word
	    if subword != word:
	       replace = reconstruct(subword, alpha_count)
	       if replace != subword and replace in word_to_pron:
		  word = word.replace(subword, replace, 1)
		  word_len = len(word)
		  i = i + len(replace) - 1 #-1 because at the end of the loop, it'll go to +1
		  break #to break from first loop... w8ing -> if it gets to w8-> wait -> waiting, we don't need to try to compare wa to something, but continue from next subset
	    j += 1

	 i += 1

      return word
      
def reconstruct_line(line):
   #we don't want to try to reconstruct strings like "2.2" because it's probably a number. Though "2. 2" is probably "to. To"
   #also since there are cases of "end of sentence. 2 something" where we don't know if 2 is supposed to be caped or not, we'll keep track of the dots too
   split_line = line.split()

   next_cap = False

   split_line_len = range(len(split_line))
   for i in split_line_len:
      #to get "clean" word, because split() would result into "end." and that dot would cause some problems
      word = split_string(split_line[i], "(?<!\.)(?:\.\s)?(?:\d?[A-Za-z]+'?\d?)+|(?<!-)(?<!#)(?<!\d\.)(?<!\d)\d(?!\d+)(?!\.\d+)(?!-)")
      dot = split_string(split_line[i], "\.")
      
      #it's probably not needed because of split but... who knows
      for element in word:   
         #do only if it's not a one capital letter character with a dot, so... b. -> yes, 4. -> yes, b4. -> yes, B. -> no... basically attempt to deal with names
         if len(dot) == 0 or len(element) != 1 or count_alpha(element) != 1 or element[0].islower():
            #don't want to translate "I" as me into "Eye"
            if element != "I":
               
               new_word = reconstruct_word(element.lower())
               if new_word != element:
                  if next_cap:
                     new_word = new_word.capitalize()

                  #could put "if count_caps() == len(new_word)" but... len(W8ING) != len(WAITING)
                  if count_caps(element) > 1:
                     tmp_word = new_word
                     new_word = ""
                     for letter in tmp_word:
                        new_word += letter.capitalize()
                  elif element[0].isupper():
                     #could put "if count_caps() > 0, but what if we have a situation of sOn
                     new_word = new_word.capitalize()

                  split_line[i] = split_line[i].replace(element, new_word, 1)
      
      #split splits in spaces, so to know if a number is supposed to be capsed or not, we need to keep reference of previous member -> "something. 2night" -> split = "something." and "2night" so "2night"
      #doesn't have a record of a dot
      next_cap = False
      if split_line[i][-1] == "." or split_line[i][-1] == "!" or split_line[i][-1] == "?" or split_line[i][-1] == ":":
	 next_cap = True

   return " ".join(split_line)

def process_file(action, read_name, output_name):
   
   read_file = open(read_name,"r")
   write_file = open(output_name, "w")
   
   for line in read_file:
      if action == "simpf":
         write_file.write(simplify_line(line) + "\n")
      else:
         write_file.write(reconstruct_line(line) + "\n")
      
   read_file.close()
   write_file.close()

if len(sys.argv) >= 3:
   action = sys.argv[1].lower()

   if action == "simply":
      print simplify_line(sys.argv[2])
   elif action == "recon":
      print reconstruct_line(sys.argv[2])
   elif action == "simpf" or action == "recf":
      if len(sys.argv) < 4:
         if action == "simpf":
            write_file = "simp_file.txt"
         else:
            write_file = "rec_file.txt"
      else:
         write_file = sys.argv[3]
      
      process_file(action, sys.argv[2], write_file)
      if action == "simpf":
         print "File " + sys.argv[2] + " has been simplified and it's output was written into " + write_file
      else:
         print "File " + sys.argv[2] + " has been reconstructed and it's output was written into " + write_file
   else:
      print 'tlas.py simply "text" - simplify the input\ntlas.py recon "text" - reconstruct the input\ntlas.py simpf input_file_name output_file_name - simplifies input file into output file. If output file name wasn\'t provided, it will be written into simp_file.txt\ntlas.py recf input_file_name output_file_name - reconstructs input file into output file. If output file name wasn\'t provided, it will be written into rec_file.txt'
else:
   print 'tlas.py simply "text" - simplify the input\n tlas.py recon "text" - reconstruct the input\ntlas.py simpf input_file_name output_file_name - simplifies input file into output file. If output file name wasn\'t provided, it will be written into simp_file.txt\ntlas.py recf input_file_name output_file_name - reconstructs input file into output file. If output file name wasn\'t provided, it will be written into rec_file.txt'