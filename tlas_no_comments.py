#This program is a submission for contest in the Udacity's CS101 class and as such is released under a Creative Commons CC BY-NC-SA license.

import re
import sys

def reverse_index(dict):
   new_dict = {}
   element_list = []
   
   for key in dict:
      if dict[key] not in element_list:
	 element_list.append(dict[key])
   for element in element_list:
      if element not in new_dict:
	 new_dict[element] = ""
      
      for key in dict:
	 if (dict[key] == element) and (key not in new_dict[element]):
	    new_dict[element] = key
   
   return new_dict

letter_to_pron = {
"a" : "ei",
"b" : "bi",
"c" : "si",
"d" : "di",
"e" : "i",
"f" : "ef",
"i" : "ai",
"k" : "kei",
"l" : "el",
"m" : "em",
"n" : "en",
"o" : "ou",
"p" : "pi",
"q" : "kyu",
"r" : "ar",
"s" : "es",
"t" : "ti",
"u" : "yu",
"v" : "vi",
"x" : "eks",
"y" : "wai",
"z" : "zi"
}
pron_to_letter = reverse_index(letter_to_pron)

number_to_pron = {
"1" : "wan",
"2" : "tu",
"4" : "for",
"5" : "faiv",
"6" : "siks",
"7" : "sevn",
"8" : "eit",
"9" : "nain"
}
pron_to_number = reverse_index(number_to_pron)

word_to_pron = {
"any" : "eni",
"are" : "ar",
"ate" : "eit",
"be" : "bi",
"before" : "bifor",
"done" : "dan",
"eye" : "ai",
"for" : "for",
"hate" : "heit",
"hatred" : "heitred",
"ing" : "in",
"mating" : "meitin",
"one" : "wan",
"see" : "si",
"some" : "sam",
"stop" : "stap",
"tea" : "ti",
"to" : "tu",
"up" : "ap",
"wait" : "weit",
"water" : "woter",
"what" : "wat",
"why" : "wai",
"you" : "yu"
}
pron_to_word = reverse_index(word_to_pron)

def split_string(source, splitlist):
   matches = re.findall(splitlist,source)
   return matches

def count_alpha(word):
   alpha_number = 0
   for character in word:
      if character.isalpha():
	 alpha_number += 1
   return alpha_number

def count_caps(word):
   caps_number = 0
   for character in word:
      if character.isupper():
         caps_number += 1
   return caps_number

def simplify_pron(word):
   pron = word_to_pron[word]
   
   if pron in pron_to_letter:
      return pron_to_letter[pron]
   if pron in pron_to_number:
      return pron_to_number[pron]
   
   pron_len = len(pron)
   
   i = 0
   while i < pron_len:
      j = 1
      while j + i < pron_len + 1:
	 if pron[i:i + j] in pron_to_number:
	    pron = pron.replace(pron[i:i + j], pron_to_number[pron[i:i + j]], 1)
	    pron_len = len(pron)
	    break
	 j += 1
      i += 1

   if count_alpha(pron) < 4:
      i = 0
      j = -1
      while i < pron_len:
	 if pron[i].isalpha() and j == -1:
	    j = i
	 elif not pron[i].isalpha() and j != -1:
	    tmp_pron = ""
	    if pron[j:i] in pron_to_letter:
	       tmp_pron = pron.replace(pron[j:i], pron_to_letter[pron[j:i]], 1)

	    if count_alpha(tmp_pron) == 1:
	       return tmp_pron
	    else:
	       break
	 i += 1
   
   i = 0
   while i < pron_len:
      j = 0
      while i + j < pron_len + 1:
	 if pron[i:i + j] in pron_to_word:
	    pron = pron.replace(pron[i:i + j], pron_to_word[pron[i:i + j]], 1)
	 j += 1
      i += 1

   return pron
   

def simplify_word(word):
   if word in word_to_pron:
      poss_candidate = simplify_pron(word)

      if poss_candidate != word:
	 return poss_candidate
      else:
	 return word
   else:
      word_len = len(word)
      i = 0
      while i < word_len:
	 j = 1
	 while j + i < word_len + 1:
	    subword = word[i:j + i]
	    if subword in word_to_pron and subword != word:
	       replace = simplify_pron(subword)
	       
	       if replace != subword:
		  digit_change = False
		  for element in replace:
		     if element.isdigit():
			digit_change = True

		  tmp_word = word.replace(subword, replace, 1)
		  if digit_change:
		     word = tmp_word
		     word_len = len(word)
		     break
	    j += 1
	 i += 1

      return word

def simplify_line(line):
   split_line = line.split()
   
   split_line_len = range(len(split_line))
   for i in split_line_len:
      word = split_string(split_line[i], "\w+'?\w")

      for element in word:
	 new_word = simplify_word(element.lower())

	 if new_word != element and new_word not in word_to_pron:
	    if count_caps(element) > 1:
               tmp_word = new_word
               new_word = ""
               for letter in tmp_word:
                  new_word += letter.capitalize()
	    elif (element[0].isupper() and new_word != "i") or element == "I":
	       new_word = new_word.capitalize()

	    split_line[i] = split_line[i].replace(element, new_word, 1)

   return " ".join(split_line)


def reconstruct(word, alpha_count = 1):
   if alpha_count == 1 and word in letter_to_pron and letter_to_pron[word] in pron_to_word:
      return pron_to_word[letter_to_pron[word]]
   if word in number_to_pron and number_to_pron[word] in pron_to_word:
      return pron_to_word[number_to_pron[word]]

   word_len = len(word)
   alpha_count = count_alpha(word)
   
   poss_cand = [word]
   if alpha_count == 1:
      i = 0
      while i < word_len:
         if word[i].isalpha():
            if word[i] in letter_to_pron:
               poss_cand.append(word.replace(word[i], letter_to_pron[word[i]], 1))
               break;
         i += 1

   poss_cand_len = len(poss_cand)
   i = 0
   while i < poss_cand_len:
      candidate = poss_cand.pop(0)
      cand_len = len(candidate)
      j = 0
      while j < cand_len:
         if candidate[j] in number_to_pron:
            poss_cand.append(candidate.replace(candidate[j], number_to_pron[candidate[j]], 1))
            if number_to_pron[candidate[j]] in pron_to_word:
               poss_cand.append(candidate.replace(candidate[j], pron_to_word[number_to_pron[candidate[j]]], 1))
         j += 1
      i += 1

   i = 0
   poss_cand_len = len(poss_cand)
   while i < poss_cand_len:
      candidate = poss_cand[i]
      cand_len = len(candidate)
      j = 0
      while j < cand_len:
         k = 0
         while k + j < cand_len + 1:
            if candidate[j:j + k] in word_to_pron:
               poss_cand.append(candidate.replace(candidate[j:j + k], word_to_pron[candidate[j:j + k]], 1))
            k += 1
         j += 1
      i += 1

   for candidate in poss_cand:
      if candidate in word_to_pron:
         return candidate
      if candidate in pron_to_word:
         return pron_to_word[candidate]
   
   return word

def reconstruct_word(word):
   poss_candidate = reconstruct(word)
   alpha_count = count_alpha(word)

   if poss_candidate != word and poss_candidate in word_to_pron:
      return poss_candidate
   else:
      word_len = len(word)
      i = 0
      while i < word_len:
	 j = 1
	 while j + i < word_len + 1:
	    subword = word[i:j + i]
	    if subword != word:
	       replace = reconstruct(subword, alpha_count)
	       if replace != subword and replace in word_to_pron:
		  word = word.replace(subword, replace, 1)
		  word_len = len(word)
		  i = i + len(replace) - 1
		  break
	    j += 1
	 i += 1

      return word
      
def reconstruct_line(line):
   split_line = line.split()

   next_cap = False

   split_line_len = range(len(split_line))
   for i in split_line_len:
      word = split_string(split_line[i], "(?<!\.)(?:\.\s)?(?:\d?[A-Za-z]+'?\d?)+|(?<!-)(?<!#)(?<!\d\.)(?<!\d)\d(?!\d+)(?!\.\d+)(?!-)")
      dot = split_string(split_line[i], "\.")
      
      for element in word:   
         if len(dot) == 0 or len(element) != 1 or count_alpha(element) != 1 or element[0].islower():
            if element != "I":
               
               new_word = reconstruct_word(element.lower())
               if new_word != element:
                  if next_cap:
                     new_word = new_word.capitalize()

                  if count_caps(element) > 1:
                     tmp_word = new_word
                     new_word = ""
                     for letter in tmp_word:
                        new_word += letter.capitalize()
                  elif element[0].isupper():
                     new_word = new_word.capitalize()
                  
                  split_line[i] = split_line[i].replace(element, new_word, 1)
      
      next_cap = False
      if split_line[i][-1] == "." or split_line[i][-1] == "!" or split_line[i][-1] == "?" or split_line[i][-1] == ":":
	 next_cap = True

   return " ".join(split_line)

def process_file(action, read_name, output_name):
   
   read_file = open(read_name,"r")
   write_file = open(output_name, "w")
   
   for line in read_file:
      if action == "simpf":
         write_file.write(simplify_line(line) + "\n")
      else:
         write_file.write(reconstruct_line(line) + "\n")
      
   read_file.close()
   write_file.close()

if len(sys.argv) >= 3:
   action = sys.argv[1].lower()

   if action == "simply":
      print simplify_line(sys.argv[2])
   elif action == "recon":
      print reconstruct_line(sys.argv[2])
   elif action == "simpf" or action == "recf":
      if len(sys.argv) < 4:
         if action == "simpf":
            write_file = "simp_file.txt"
         else:
            write_file = "rec_file.txt"
      else:
         write_file = sys.argv[3]
      
      process_file(action, sys.argv[2], write_file)
      if action == "simpf":
         print "File " + sys.argv[2] + " has been simplified and it's output was written into " + write_file
      else:
         print "File " + sys.argv[2] + " has been reconstructed and it's output was written into " + write_file
   else:
      print 'tlas.py simply "text" - simplify the input\ntlas.py recon "text" - reconstruct the input\ntlas.py simpf input_file_name output_file_name - simplifies input file into output file. If output file name wasn\'t provided, it will be written into simp_file.txt\ntlas.py recf input_file_name output_file_name - reconstructs input file into output file. If output file name wasn\'t provided, it will be written into rec_file.txt'
else:
   print 'tlas.py simply "text" - simplify the input\n tlas.py recon "text" - reconstruct the input\ntlas.py simpf input_file_name output_file_name - simplifies input file into output file. If output file name wasn\'t provided, it will be written into simp_file.txt\ntlas.py recf input_file_name output_file_name - reconstructs input file into output file. If output file name wasn\'t provided, it will be written into rec_file.txt'