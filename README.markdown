# Typing Like a Sir
This program is a submission for contest in the [Udacity's](http://www.udacity.com/) CS101 class and as such is released under a Creative Commons CC BY-NC-SA license.

## That's cool but, what does it do?
Simplest example:

    Wait for me => W8 4 me
    Someone has to go tonight => Some1 has 2 go 2night

And no, it's not just a simple converter that does

    string = string.replace("to", "2")
    string = string.replace("for", "4")

and vice versa.

Parameters it takes:

"simply" to simplify the message:

    tlas.py simply "Someone has to go tonight"
    Some1 has 2 go 2night

"recon" to reconstruct the message:

    tlas.py recon "What r u up 2"
    What are you up to

"simpf" to simplify the entire file:

    tlas.py simpf input_file_name output_file_name
    
If output file name wasn't provided, it'll save it to "simp_file.txt"

"recf" to reconstruct the entire file:

    tlas.py recf input_file_name output_file_name

If output file name wasn't provided, it'll save it to "rec_file.txt"

If you are interested in how it works, where it could be used and some other info, check wiki, either that or this would be a very long README









